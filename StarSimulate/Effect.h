#pragma once

#include <random>
#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx11.h"

class Effect
{
private:
	ImVec2 _window;
	std::mt19937 rng;
	std::uniform_int_distribution<int> distrX;
	std::uniform_int_distribution<int> distrY;
	ImVec2 pos;
	ImVec2 next_pos;
public:
	Effect() {};

	Effect(ImVec2 window, std::mt19937& rng): _window(window), rng(rng) {
		distrX.param(std::uniform_int_distribution<int>::param_type(0, window.x));
		distrY.param(std::uniform_int_distribution<int>::param_type(0, window.y));
		pos.x = distrX(rng);
		pos.y = distrY(rng);
		next_pos.x = distrX(rng);
		next_pos.y = distrY(rng);
	}

	void Update() {
		static double stepX = (next_pos.x - pos.x) / 2000;
		static double stepY = (next_pos.y - pos.y) / 2000;

		if ( sqrt(pow((pos.x-next_pos.x),2)+pow((pos.y-next_pos.y), 2)) > 2) {
			pos.x += stepX;
			pos.y += stepY;
		}
		else {
			pos = next_pos;
			next_pos.x = distrX(rng);
			next_pos.y = distrY(rng);

			stepX = (next_pos.x - pos.x) / 3000;
			stepY = (next_pos.y - pos.y) / 3000;
		}
	};

	void Draw(ImDrawList* drawList) {
		this->Update();
		drawList->AddCircleFilled(pos, 2.0f, ImColor(255, 255, 255), 25);
	};

};


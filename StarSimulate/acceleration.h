﻿#pragma once

#include "matrix.hpp"
#include "high_math.h"
#include "coordinate_system.h"
#include "sky_object.h"
#include "Sattelite.h"

class acceleration_Earth {
private:
	const double _fm = 398600.44150;
	const double _r_0= 6378.13630;
	const int _N_max = 12;
	Matrix<double> _C, _S;
	Matrix<double> R, Z, Q;
	Matrix<double> X, Y;
	Matrix<double> dX_dx, dX_dy, dY_dx, dY_dy;
	Matrix<double> dQ_dx, dQ_dy;
	Matrix<double> dR;

	void calculate_R_n(double _r) noexcept;

	void calculate_dR_n(double _r) noexcept;

	void calculate_Z_nk(double _z, double _r) noexcept;

	void calculate_XY_k(double _x, double _y, double _r) noexcept;

	void calculate_Q_nk(double _x, double _y, double _r) noexcept;

	void calculate_dX_dY(double _x, double _y, double _r) noexcept;

	void calculate_dQ(double _x, double _y, double _r) noexcept;

public:
	acceleration_Earth() noexcept;

	sky_system get_acceleration_Earth(sky_system& coord_ESA) noexcept;
};

class acceleration_Moon {
private:
	const double _fm_M = 4.902799E3;
	std::unique_ptr<sky_object::Moon> _moon;
public:
	acceleration_Moon(): _moon(new sky_object::Moon) {};

	sky_system get_acceleration_Moon(double time_TDB, sky_system& coord_ESA) {

		ecliptic_system coord = _moon->coord_Moon(time_TDB);
		sky_system coord_Moon = sky_system{ coord,time_TDB };
		Matrix<double> coord_Moon_rect = coord_Moon.get_rectange();

		double X_M = coord_Moon_rect.at(0, 0);
		double Y_M = coord_Moon_rect.at(1, 0);
		double Z_M = coord_Moon_rect.at(2, 0);

		double r_M = sqrt(X_M * X_M + Y_M * Y_M + Z_M * Z_M);

		Matrix<double> coord_ESA_rect = coord_ESA.get_rectange();

		double x_c = coord_ESA_rect.at(0, 0);
		double y_c = coord_ESA_rect.at(1, 0);
		double z_c = coord_ESA_rect.at(2, 0);

		double r_0 = sqrt(pow(X_M - x_c, 2) + pow(Y_M - y_c, 2) + pow(Z_M - z_c, 2));

		double F_Mx = _fm_M * ((X_M - x_c) / pow(r_0, 3) - X_M / pow(r_M, 3));
		double F_My = _fm_M * ((Y_M - y_c) / pow(r_0, 3) - Y_M / pow(r_M, 3));
		double F_Mz = _fm_M * ((Z_M - z_c) / pow(r_0, 3) - Z_M / pow(r_M, 3));

		sky_system output{0,0,0};
		output.set_rectange(Matrix<double>(3, 1, { F_Mx, F_My, F_Mz }));
		output.rectangle2sphere();
		return output;
	}
};

class acceleration_Sun {
private:
	const double _fm_S = 1.32712438E11;
	std::unique_ptr<sky_object::Sun> _sun;
public:
	acceleration_Sun() : _sun(new sky_object::Sun) {};

	sky_system get_acceleration_Sun(double time_TDB, sky_system& coord_ESA) {

		ecliptic_system coord = _sun->coord_Sun(time_TDB);
		sky_system coord_Sun = sky_system{ coord , time_TDB};
		Matrix<double> coord_Sun_rect = coord_Sun.get_rectange();

		double X_S = coord_Sun_rect.at(0, 0);
		double Y_S = coord_Sun_rect.at(1, 0);
		double Z_S = coord_Sun_rect.at(2, 0);

		double r_S = sqrt(X_S * X_S + Y_S * Y_S + Z_S * Z_S);

		Matrix coord_ESA_rect = coord_ESA.get_rectange();

		double x_c = coord_ESA_rect.at(0, 0);
		double y_c = coord_ESA_rect.at(1, 0);
		double z_c = coord_ESA_rect.at(2, 0);

		double r_0 = sqrt(pow(X_S - x_c, 2) + pow(Y_S - y_c, 2) + pow(Z_S - z_c, 2));

		double F_Sx = _fm_S * ((X_S - x_c) / pow(r_0, 3) - X_S / pow(r_S, 3));
		double F_Sy = _fm_S * ((Y_S - y_c) / pow(r_0, 3) - Y_S / pow(r_S, 3));
		double F_Sz = _fm_S * ((Z_S - z_c) / pow(r_0, 3) - Z_S / pow(r_S, 3));

		sky_system output{0,0,0};
		output.set_rectange(Matrix<double>(3, 1, { F_Sx, F_Sy, F_Sz }));
		output.rectangle2sphere();
		return output;
	}
};

class acceleration_Light {
private:
	const double P_0 = 4.5606E-6;
	double C_refl;
	std::unique_ptr<sky_object::Sun> _sun;
public:
	acceleration_Light(double k_r, double A_s, double m_s): _sun(new sky_object::Sun) {
		C_refl = 10E-3 * P_0 * k_r * A_s / m_s;
	}

	sky_system get_acceleration_Light(double time_TDB, sky_system& coord_ESA) {

		ecliptic_system coord = _sun->coord_Sun(time_TDB-0.0057755);
		sky_system coord_Sun = sky_system{coord, time_TDB};

		Matrix<double> coord_Sun_rect = coord_Sun.get_rectange();

		double X_S = coord_Sun_rect.at(0, 0);
		double Y_S = coord_Sun_rect.at(1, 0);
		double Z_S = coord_Sun_rect.at(2, 0);

		Matrix<double> coord_ESA_rect = coord_ESA.get_rectange();

		double x_c = coord_ESA_rect.at(0, 0);
		double y_c = coord_ESA_rect.at(1, 0);
		double z_c = coord_ESA_rect.at(2, 0);

		double r_0 = sqrt(pow(X_S - x_c, 2) + pow(Y_S - y_c, 2) + pow(Z_S - z_c, 2));

		double F_px = C_refl* (1.4959787E8 / r_0) * (1.4959787E8 / r_0) * (x_c - X_S) / r_0;
		double F_py = C_refl* (1.4959787E8 / r_0) * (1.4959787E8 / r_0) * (y_c - Y_S) / r_0;
		double F_pz = C_refl* (1.4959787E8 / r_0) * (1.4959787E8 / r_0) * (z_c - Z_S) / r_0;

		sky_system output{0,0,0};
		output.set_rectange(Matrix<double>(3, 1, { F_px, F_py, F_pz }));
		output.rectangle2sphere();
		return output;
	}
};

class acceleration_Atmosphere {
private:
	const double r_0 = 6378.14;
	const double α_0 = 1.0 / 298.257;
	double S_b;
public:
	acceleration_Atmosphere(double A_s, double m_s) {
		S_b = 1E6 * A_s / m_s;
	};

	sky_system get_acceleration_Atmosphere(sky_system& r, sky_system& v) {

		Matrix<double> r_rect = r.get_rectange();
		Matrix<double> v_rect = v.get_rectange();

		double x_c = r_rect.at(0, 0);
		double y_c = r_rect.at(0, 1);
		double z_c = r_rect.at(0, 2);

		double v_c_x = v_rect.at(0, 0);
		double v_c_y = v_rect.at(0, 1);
		double v_c_z = v_rect.at(0, 2);

		double r_c = sqrt(pow(x_c, 2) + pow(y_c, 2) + pow(z_c, 2));
		double v_c = sqrt(pow(v_c_x, 2) + pow(v_c_y, 2) + pow(v_c_z, 2));

		double h = r_c - r_0 * (1-α_0*(z_c/r_c)*(z_c/r_c));
		double ρ = 2E-13 * exp(( - h + 200) / 60);

		double F_ax = -S_b * ρ * v_c * v_c_x;
		double F_ay = -S_b * ρ * v_c * v_c_y;
		double F_az = -S_b * ρ * v_c * v_c_z;

		sky_system output{0,0,0};

		output.set_rectange(Matrix<double>(3,1,{ F_ax, F_ay, F_az }));
		output.rectangle2sphere();

		return output;
	}
};

class Acceleration {
private:
	acceleration_Earth acc_earth;
	acceleration_Moon acc_moon;
	acceleration_Sun acc_sun;
	acceleration_Light acc_light;
	acceleration_Atmosphere acc_atm;
public:
	Acceleration(double k_r, double A_s, double m_s) noexcept : acc_atm(A_s, m_s), acc_light(k_r, A_s, m_s), acc_sun(), acc_moon(), acc_earth() {};

	Matrix<double> operator()(Matrix<double> sat,double time_TDB) {

		sky_system sat_sky{ 0,0,0 };
		sat_sky.set_rectange(sat);
		sat_sky.rectangle2sphere();

		sky_system v{ 1,1,1 };

		sky_system acc_a = acc_atm.get_acceleration_Atmosphere(sat_sky, v);
		sky_system acc_l = acc_light.get_acceleration_Light(time_TDB, sat_sky);
		sky_system acc_s = acc_sun.get_acceleration_Sun(time_TDB, sat_sky);
		sky_system acc_m = acc_moon.get_acceleration_Moon(time_TDB, sat_sky);
		sky_system acc_e = acc_earth.get_acceleration_Earth(sat_sky);

		sky_system& tmp = (acc_a + acc_l + acc_s + acc_m + acc_e);

		return tmp.get_rectange();
	}
};

Matrix<double> acc(Matrix<double> sat, double time_TDB);
#pragma once

#include "ChiliWin.h"
#include "my_exception.h"
#include "DxgiInfoManager.h"
#include <vector>
#include <d3d11.h>
#include <wrl.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <memory>
#include <random>
#include "GraphicsThrowMacros.h"

class Graphics {
	friend class Bindable;
public:
	class Exception : public my_exception {
		using my_exception::my_exception;
	};
	class HrException : public Exception {
	public:
		HrException(int line, const char* file, HRESULT hr, std::vector<std::string> infoMsgs = {}) noexcept;
		const char* what() const noexcept override;
		const char* GetType() const noexcept override;
		HRESULT GetErrorCode() const noexcept;
		std::string GetErrorString() const noexcept;
		std::string GetErrorDescription() const noexcept;
		std::string GetErrorInfo() const noexcept;
	private:
		HRESULT hr;
		std::string info;
	};
	class DeviceRemovedException : public HrException {
		using HrException::HrException;
	public:
		const char* GetType() const noexcept override;
	private:
		std::string reason;
	};
	class InfoException : public Exception {
	public:
		InfoException(int line, const char* file,std::vector<std::string> infoMsgs) noexcept;
		const char* what() const noexcept override;
		const char* GetType() const noexcept override;
		std::string GetErrorInfo() const noexcept;
	private:
		std::string info;
	};
public:
	Graphics() {};
	Graphics(HWND hWnd);
	Graphics(const Graphics&) = delete;
	Graphics operator=(const Graphics&) = delete;
	~Graphics();

	void EndFrame();
	void BeginFrame(float red, float green, float blue) noexcept;
	void DrawIndexed(UINT count) noexcept(!IS_DEBUG);
	void SetProjection(DirectX::FXMMATRIX proj) noexcept;
	DirectX::XMMATRIX GetProjection() const noexcept;
	void SetCamera(DirectX::FXMMATRIX cam) noexcept;
	DirectX::FXMMATRIX GetCamera() const noexcept;

	//Texture Render
	void SetRenderTarget() {
		pContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), pDSV.Get());
	}
	
	void ReturnRenderTarget() {
		pContext->OMSetRenderTargets(1u, pTarget.GetAddressOf(), pDSV.Get());
	}

	//Load Image
	void LoadBackgroundFromFile(const char* filename, ID3D11ShaderResourceView** out_srv, int* out_width, int* out_height);

	void EnableImGui() noexcept;
	void DisableImGui() noexcept;
	bool IsImGuiEnabled() noexcept;
private:
	DirectX::XMMATRIX projection;
	DirectX::XMMATRIX camera;
	bool imguiEnabled = true;
#ifndef NDEBUG
	DxgiInfoManager infoManager;
#endif

private:
	Microsoft::WRL::ComPtr<ID3D11Device> pDevice;
	Microsoft::WRL::ComPtr<IDXGISwapChain> pSwap;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> pContext;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> pTarget;	
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDSV;

//Render to Texture
	Microsoft::WRL::ComPtr<ID3D11Texture2D> m_renderTargetTexture;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
public:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResourceView;
};
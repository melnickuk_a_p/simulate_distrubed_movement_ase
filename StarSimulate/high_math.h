#pragma once
#include <math.h>

constexpr float PI = 3.14159265f;
constexpr double PI_D = 3.1415926535897932;

template <typename T>
constexpr auto sq(const T& x)
{
	return x * x;
}

template<typename T>
T wrap_angle(T theta)
{
	const T modded = fmod(theta, (T)2.0 * (T)PI_D);
	return (modded > (T)PI_D) ?
		(modded - (T)2.0 * (T)PI_D) :
		modded;
}

template<typename T>
constexpr T interpolate(const T& src, const T& dst, float alpha)
{
	return src + (dst - src) * alpha;
}

template<typename T>
constexpr T to_rad(T deg)
{
	return deg * PI / (T)180.0;
}


inline double Legendre(double arg, int n) {
	double P_n_0 = 1;
	double P_n_1 = arg;
	double P_n_2;

	for (uint32_t count = 1; count < n; ++count) {
		P_n_2 = ((2 * count + 1) * arg * P_n_1 - count * P_n_0)/(count+1);
		P_n_0 = P_n_1;
		P_n_1 = P_n_2;
	}

	if (n == 0)
		return P_n_0;
	if (n== 1)
		return P_n_1;
	if (n >= 2)
		return P_n_2;
}

inline double Legendre_n_1(double arg, int n) {
	double _P_0 = 0;
	double _P_1 = 1;
	double _P_2 = 3 * arg;
	double _P_3;
	for (uint32_t count = 3; count < n+1; ++count) {
		_P_3 = count * Legendre(arg, count - 1) + arg * _P_2;
		if (count != n) {
			_P_2 = _P_3;
		}
	}

	if (n == 0)
		return _P_0;
	if ( n == 1)
		return _P_1;
	if (n == 2)
		return _P_2;
	if (n >= 3)
		return _P_3;
}

inline double Legendre_n_k(double arg, int n, int k) {

	
	if (k == 0)
		return Legendre(arg, n);

	if (k == 1)
		return Legendre_n_1(arg, n);

	if (n < k)
		return 0;

	return (2 * n - 1) * Legendre_n_k(arg, n - 1, k - 1) + Legendre_n_k(arg, n - 2, k);

}
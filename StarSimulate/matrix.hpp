﻿#pragma once
#include <vector>
#include <cmath>
#include <cassert>
#include <iostream>

template<typename T>
class Matrix {
private:
	uint32_t _cols;
	uint32_t _rows;
	std::vector<T> _vals;
public:
	Matrix() noexcept
	{
		_cols = 1;
		_rows = 1;
		_vals.push_back(0);
	}

	Matrix(uint32_t rows, uint32_t cols) noexcept : _cols(cols), _rows(rows), _vals({}) {
		_vals.resize(cols * rows, T{});
	}

	Matrix(uint32_t rows, uint32_t cols, std::vector<T> vals) noexcept : Matrix<T>(rows, cols)
	{
		this->_vals = vals;
	}

	T& at(uint32_t row, uint32_t col) {
		return _vals.at(_cols * row + col);
	}

	Matrix<T>& multiply(Matrix<T>& target) {

		Matrix<T> temp(this->_rows, target._cols);

		for (uint32_t row = 0; row < this->_rows; ++row) {
			for (uint32_t col = 0; col < target._cols; ++col) {
				for (uint32_t row2 = 0; row2 < target._rows; ++row2)
					temp.at(row, col) += this->at(row, row2) * target.at(row2, col);
			}
		}

		*this = temp;

		return *this;
	}

	Matrix<T>& add(Matrix<T>& target) {
		assert(_rows == target._rows && _cols == target._cols);

		for (uint32_t y = 0; y < _rows; ++y) {
			for (uint32_t x = 0; x < _cols; ++x) {
				this->at(x, y) += target.at(x, y);
			}
		}

		return *this;
	}

	Matrix<T>& multiply_scaler(double s) {

		for (uint32_t y = 0; y < _rows; ++y) {
			for (uint32_t x = 0; x < _cols; ++x) {
				at(y,x) = at(y,x) * s;
			}
		}
		return *this;
	}

	Matrix<T>& add_scaler(double s) {

		for (uint32_t y = 0; y < _rows; ++y) {
			for (uint32_t x = 0; x < _cols; ++x) {
				at(x, y) = at(x, y) + s;
			}
		}
		return *this;
	}

	Matrix<T> negetive() {
		Matrix<T> output(_rows, _cols);
		for (uint32_t y = 0; y < output._rows; ++y) {
			for (uint32_t x = 0; x < output._cols; ++x) {
				output.at(x, y) = -at(x, y);
			}
		}
		return output;
	}

	Matrix<T>& transpose() noexcept {
		Matrix<T> temp(this->_cols, this->_rows);

		for (uint32_t row = 0; row < this->_cols; ++row) {
			for (uint32_t col = 0; col < this->_rows; ++col) {
				temp.at(row, col) = this->at(col, row);
			}
		}

		*this = temp;

		return *this;
	}

	friend Matrix<double> get_rot_x(double α);

	friend Matrix<double> get_rot_y(double α);

	friend Matrix<double> get_rot_z(double α);

	template<typename T>
	friend std::ostream& operator<<(std::ostream& out, Matrix<T>& mat);

	// Умножает матрицу и изменяет объект
	T& operator()(uint32_t col, uint32_t row)
	{
		return this->at(col, row);
	}

	Matrix<T>& operator*(Matrix<T>& mat)
	{
		return this->multiply(mat);
	}

	Matrix<T>& operator+(Matrix<T>& mat)
	{
		return this->add(mat);
	}

	Matrix<T>& operator+(double scalar)
	{
		return this->add_scaler(scalar);
	}

	Matrix<T>& operator-(double scalar)
	{
		return this->add_scaler(-scalar);
	}

	Matrix<T>& operator*(double scalar)
	{
		return this->multiply_scaler(scalar);
	}

	Matrix<T>& operator/(double scalar)
	{
		return this->multiply_scaler(1 / scalar);
	}
};

inline Matrix<double> get_rot_x(double α) {
	Matrix<double> Rot_x(3, 3);
	Rot_x._vals = std::vector<double>({ 1,0,0,0,cos(α), sin(α),0,-sin(α), cos(α) });
	return Rot_x;
}

inline Matrix<double> get_rot_z(double α) {
	Matrix<double> Rot_y(3, 3);
	Rot_y._vals = std::vector<double>({ cos(α), sin(α),0,-sin(α),cos(α),0,0,0,1 });
	return Rot_y;
}

inline Matrix<double> get_rot_y(double α) {
	Matrix<double> Rot_z(3, 3);
	Rot_z._vals = std::vector<double>({ cos(α), 0 , -sin(α),0,1,0,sin(α),0,cos(α) });
	return Rot_z;
}

template<typename T>
inline std::ostream& operator<<(std::ostream& out, Matrix<T>& mat)
{
	for (uint32_t row = 0; row < mat._rows; ++row) {
		for (uint32_t col = 0; col < mat._cols; ++col) {
			out << mat.at(row, col) << " ";
		}
		out << "\n";
	}

	return out;
}

template<typename T, typename S>
inline Matrix<T>& operator*(S s, Matrix<T>& mat){
	return mat.multiply_scaler(s);
}

template<typename T, typename S>
inline Matrix<T>& operator*(Matrix<T>& mat, S s) {
	return mat.multiply_scaler(s);
}

template<typename T>
inline Matrix<T>& operator+=(Matrix<T>& lmat, Matrix<T>& rmat) {
	return lmat.add(rmat);
}

template<typename T, typename S>
inline Matrix<T>& operator/(Matrix<T>& mat, S s) {
	return mat.multiply_scaler(1 / s);
}

template<typename T>
inline Matrix<T>& operator-(Matrix<T>& rmat, Matrix<T>& lmat) {
	Matrix<T> buf = lmat.negetive();
	return rmat.add(buf);
}
#include "acceleration.h"

acceleration_Earth::acceleration_Earth() noexcept:
	R(_N_max + 1, 1),
	Z(_N_max + 2, _N_max + 2),
	X(_N_max + 1, 1), Y(_N_max + 1, 1),
	Q(_N_max + 1, _N_max + 1),
	dX_dx(_N_max + 1, 1), dX_dy(_N_max + 1, 1),
	dY_dx(_N_max + 1, 1), dY_dy(_N_max + 1, 1),
	dQ_dx(_N_max + 1, _N_max + 1), dQ_dy(_N_max + 1, _N_max + 1),
	dR(_N_max + 1, 1),
	_C(13, 13, { 0,0,0,0,0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,0,0,0,0,
					-1082.636022982994350E-6,-2.41399954E-10,1.57453604E-6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					2.532435345786144E-6, 2.19279880E-6, 3.09016045E-7, 1.00558857E-7, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					1.619331205200000E-6, -5.08725304E-7,7.84122308E-8,5.92157432E-8,-3.98239574E-9, 0, 0, 0, 0, 0, 0 ,0, 0,
					0.227716101681514E-6, -5.37165102E-8, 1.05590535E-7, -1.49261539E-8, -2.29791235E-9, 4.30476750E-10, 0, 0, 0, 0, 0, 0, 0,
					-0.539648490548677E-6, -5.98779769E-8, 6.01209884E-9, 1.18226641E-9, -3.26413891E-10, -2.15577115E-10, 2.21369255E-12, 0, 0, 0, 0 ,0, 0,
					0.351368441935748E-6, 2.05148728E-7, 3.28449048E-8, 3.52854052E-9, -5.85119492E-10, 5.81848555E-13, -2.49071768E-11, 2.55907810E-14, 0, 0, 0, 0, 0,
					0.202518715313026E-6, 1.60345872E-8, 6.57654233E-9, -1.94635815E-10, -3.18935802E-10, -4.61517343E-12, -1.83936427E-12, 3.42976182E-13, -1.58033229E-13, 0,0,0, 0,
					0.119368713461697E-6, 9.24192722E-8, 1.56687424E-9, -1.21727527E-9, -7.01856112E-12, -1.66973699E-12, 8.29672520E-13, -2.25197343E-13,6.14439391E-14, -3.67676273E-15, 0,0,0,
					0.248056864821947E-6, 5.17557870E-8, -5.56284564E-9, -4.19599893E-11, -4.96702541E-11, -3.07428287E-12, -2.59723183E-13, 6.90915376E-15, 4.63531420E-15, 2.33014817E-15, 4.17080240E-16, 0,0,
					-0.240565213809487E-6, 9.50842760E-9, 9.54203028E-10, -1.40960772E-10, -1.68525661E-11, 1.48944116E-12, -5.75467116E-15, 1.95426202E-15, -2.92494873E-16, -1.93432044E-16, -4.94639649E-17, 9.35170551E-18,0,
					0.181911703000000E-6, -3.06800094E-8, 6.38039765E-10, 1.45191793E-10, -2.12381469E-11, 8.27990199E-13, 7.88309139E-15, -4.13155736E-15,-5.70825414E-16, 1.01272849E-16, -1.84017258E-18, 4.97869995E-19, -2.10894892E-20, }),
	_S(13, 13, { 0,0,0,0,0,0,0,0,0,0,0,0,0,
				   0,0,0,0,0,0,0,0,0,0,0,0,0,
					0, 1.54309997E-9, -9.03868073E-7, 0,0,0,0,0,0,0,0,0,0,
					0, 2.68011894E-7, -2.11402398E-7, 1.97201324E-7, 0,0,0,0,0,0,0,0,0,
					0, -4.49459935E-7, 1.48155457E-7, -1.20112918E-8, 6.52560581E-9, 0,0,0,0,0,0,0,0,
					0, -8.06634638E-8, -5.23267240E-8, -7.10087714E-9, 3.87300507E-10, -1.64820395E-9, 0,0,0,0,0,0,0,
					0, 2.11646643E-8, -4.65039481E-8, 1.84313369E-10, -1.78449133E-9, -4.32918170E-10, -5.52771222E-11, 0, 0, 0 ,0, 0 , 0,
					0, 6.93698935E-8, 9.28231439E-9, -3.06115024E-9, -2.63618222E-10, 6.39725265E-12, 1.05348786E-11, 4.47598342E-13, 0, 0, 0, 0, 0,
					0, 4.01997816E-8, 5.38131641E-9, -8.72351950E-10, 9.11773560E-11, 1.61252083E-11, 8.62774317E-12, 3.81476567E-13, 1.53533814E-13, 0, 0, 0, 0,
					0, 1.42365696E-8, -2.22867920E-9, -5.63392145E-10, 1.71730872E-11, -5.55091854E-12, 2.94031315E-12, -1.84679217E-13, -9.85618446E-16, 7.44103881E-15, 0, 0, 0,
					0, -8.12891488E-8, -3.05712916E-9, -8.98933286E-10, -4.62248271E-11, -3.12226930E-12, -5.51559139E-13, -2.65068061E-15, -1.05284266E-14, -7.01194816E-16, -9.89260955E-17, 0, 0,
					0, -1.64654645E-8, -5.09736032E-9, -6.86352078E-10, -2.67779792E-11, 1.98250517E-12, 1.34623363E-13, -3.72803733E-14, 1.17044830E-15, 2.58524487E-16, -1.73664923E-17, -1.40785570E-17,0,
					0, -2.37844845E-8, 1.41642228E-9, 9.15457482E-11, 9.17051709E-13, 2.03324862E-12, 9.33540765E-14, 7.89991291E-15, 3.70152251E-16, 6.13664388E-17, 9.24242436E-18, -2.79007835E-19, -9.83829860E-20 }) {};

void acceleration_Earth::calculate_R_n(double _r) noexcept {
	R(2, 0) = _fm / _r * (_r_0 / _r) * (_r_0 / _r);

	for (uint32_t count = 3; count <= _N_max; ++count) {
		R(count, 0) = R(count - 1, 0) * (_r_0 / _r);
	}
}

void acceleration_Earth::calculate_dR_n(double _r) noexcept {
	for (uint32_t count = 2; count <= _N_max; ++count) {
		dR(count, 0) = (count + 1) * _fm * pow((_r_0 / _r), count);
	}
}

void acceleration_Earth::calculate_Z_nk(double _z, double _r) noexcept {
	for (uint32_t count = 0; count <= _N_max + 1; ++count) {
		Z(count, 0) = Legendre(_z / _r, count);
		Z(count, 1) = Legendre_n_1(_z / _r, count);
		for (uint32_t k = 2; k < +_N_max; ++k)
			Z(count, k) = Legendre_n_k(_z / _r, count, k);
	}
}

void acceleration_Earth::calculate_XY_k(double _x, double _y, double _r) noexcept {
	X(0, 0) = 1;
	Y(0, 0) = 0;
	for (uint32_t k = 1; k <= _N_max; ++k) {
		X(k, 0) = X(k - 1, 0) * _x / _r - Y(k - 1, 0) * _y / _r;
		Y(k, 0) = Y(k - 1, 0) * _x / _r + X(k - 1, 0) * _y / _r;
	}
}

void acceleration_Earth::calculate_Q_nk(double _x, double _y, double _r) noexcept {
	calculate_XY_k(_x, _y, _r);

	for (uint32_t n = 0; n <= _N_max; ++n) {
		for (uint32_t k = 0; k <= _N_max; ++k) {
			Q(n, k) = _C(k, n) * X(k, 0) + _S(k, n) * Y(k, 0);
		}
	}
}

void acceleration_Earth::calculate_dX_dY(double _x, double _y, double _r) noexcept {
	dX_dx(0, 0) = 0;
	dX_dy(0, 0) = 0;
	dY_dx(0, 0) = 0;
	dY_dy(0, 0) = 0;

	for (uint32_t k = 1; k <= _N_max; ++k) {
		dX_dx(k, 0) = dX_dx(k - 1, 0) * _x / _r + X(k - 1, 0) - dY_dx(k - 1, 0) * _y / _r;
		dX_dy(k, 0) = dX_dy(k - 1, 0) * _x / _r - dY_dy(k - 1, 0) * _y / _r - Y(k - 1, 0);
		dY_dx(k, 0) = dY_dx(k - 1, 0) * _x / _r + Y(k - 1, 0) + dX_dx(k - 1, 0) * _y / _r;
		dY_dy(k, 0) = dY_dy(k - 1, 0) * _x / _r + dX_dy(k - 1, 0) * _y / _r + X(k - 1, 0);
	}
}

void acceleration_Earth::calculate_dQ(double _x, double _y, double _r) noexcept{
	calculate_dX_dY(_x, _y, _r);

	for (uint32_t n = 0; n <= _N_max; ++n) {
		for (uint32_t k = 0; k <= _N_max; ++k) {
			dQ_dx(n, k) = _C(k, n) * dX_dx(k, 0) + _S(k, n) * dY_dx(k, 0);
			dQ_dy(n, k) = _C(k, n) * dX_dy(k, 0) + _S(k, n) * dY_dy(k, 0);
		}
	}
}

sky_system acceleration_Earth::get_acceleration_Earth(sky_system& coord_ESA) noexcept {
	
	Matrix<double> coord = coord_ESA.get_rectange();
	double _x = coord.at(0, 0);
	double _y = coord.at(0, 1);
	double _z = coord.at(0, 2);
	
	double _r = sqrt(sq(_x) + sq(_y) + sq(_z));
		
	double F_x = -_fm * _x / (_r * _r * _r);
	double F_y = -_fm * _y / (_r * _r * _r);
	double F_z = -_fm * _z / (_r * _r * _r);

	this->calculate_R_n(_r);
	this->calculate_dR_n(_r);
	this->calculate_Z_nk(_z, _r);	
	this->calculate_Q_nk(_x, _y, _r);
	this->calculate_dQ(_x, _y, _r);

	double r_3 = _r * _r * _r;

	Matrix<double> dx(4, 1, { -_x / r_3 , 1 / _r - _x * _x / r_3 , -_x * _y / r_3 , -_x * _z / r_3 });
	Matrix<double> dy(4, 1, { -_y / r_3 , -_x * _y / r_3 , 1 / _r - _y * _y / r_3 , -_y * _z / r_3 });
	Matrix<double> dz(4, 1, { -_z / r_3 , -_x * _z / r_3 , -_y * _z / r_3 , 1 / _r - _z * _z / r_3 });

	for (uint32_t n = 2; n <= _N_max; ++n) {
		for (uint32_t k = 0; k <= n; ++k) {
			F_x += dR(n, 0) * dx(0,0) * Z(n, k) * Q(n, k) +
				R(n, 0) * Z(n, k + 1) * dx(3,0) * Q(n, k) +
				R(n, 0) * Z(n, k) * (dQ_dx(n, k) * dx(1,0) + dQ_dy(n, k) * dx(2,0));
			F_y += dR(n, 0) * dy(0,0) * Z(n, k) * Q(n, k) +
				R(n, 0) * Z(n, k + 1) * dy(3,0) * Q(n, k) +
				R(n, 0) * Z(n, k) * (dQ_dx(n, k) * dy(1,0) + dQ_dy(n, k) * dy(2,0));
			F_z += dR(n, 0) * dz(0,0) * Z(n, k) * Q(n, k) +
				R(n, 0) * Z(n, k + 1) * dz(3,0) * Q(n, k) +
				R(n, 0) * Z(n, k) * (dQ_dx(n, k) * dz(1,0) + dQ_dy(n, k) * dz(2,0));
		}
	}
	
	sky_system output{0,0,0};
	output.set_rectange(Matrix<double>(3, 1, { F_x, F_y, F_z }));
	output.rectangle2sphere();

	return output;
}

Matrix<double> acc(Matrix<double> sat, double time_TDB) {
	static acceleration_Earth acc_earth;
	static acceleration_Moon acc_moon;
	static acceleration_Sun acc_sun;
	static acceleration_Light acc_light(1.1, 2, 400);
	static acceleration_Atmosphere acc_atm(2, 400);

	static sky_system sat_sky{ 0,0,0 };
	sat_sky.set_rectange(sat);
	sat_sky.rectangle2sphere();

	static sky_system v{ 0,0,0 };

	sky_system acc_a = acc_atm.get_acceleration_Atmosphere(sat_sky, v);
	sky_system acc_l = acc_light.get_acceleration_Light(time_TDB, sat_sky);
	sky_system acc_s = acc_sun.get_acceleration_Sun(time_TDB, sat_sky);
	sky_system acc_m = acc_moon.get_acceleration_Moon(time_TDB, sat_sky);
	sky_system acc_e = acc_earth.get_acceleration_Earth(sat_sky);

	sky_system& tmp = (acc_a + acc_l +acc_s + acc_m + acc_e);

	return tmp.get_rectange();
}
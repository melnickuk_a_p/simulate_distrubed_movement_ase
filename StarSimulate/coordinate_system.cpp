﻿#include "coordinate_system.h"

coordinate_system::coordinate_system() noexcept {};

coordinate_system::coordinate_system(double _α, double _β, double _r) noexcept:
	sphere_coord(1,3, std::vector<double>{_α, _β, _r}), rectangle_coord(1,3), _earth(new sky_object::Earth)
{	
	sphere2rectangle();
};

void coordinate_system::sphere2rectangle() noexcept
{
	rectangle_coord.at(0, 0) = sphere_coord.at(0, 2) * cos(sphere_coord.at(0, 1)) * cos(sphere_coord.at(0, 0));
	rectangle_coord.at(0, 1) = sphere_coord.at(0, 2) * cos(sphere_coord.at(0, 1)) * sin(sphere_coord.at(0, 0));
	rectangle_coord.at(0, 2) = sphere_coord.at(0, 2) * sin(sphere_coord.at(0, 1));
};

void coordinate_system::rectangle2sphere() noexcept
{
	sphere_coord.at(0, 0) = atan2(rectangle_coord.at(0, 1), rectangle_coord.at(0, 0));
	sphere_coord.at(0, 1) = acos(rectangle_coord.at(0,2)/sqrt(pow(rectangle_coord.at(0,0),2)+ pow(rectangle_coord.at(0, 1), 2) + pow(rectangle_coord.at(0,2), 2)));
	sphere_coord.at(0, 2) = sqrt(pow(rectangle_coord.at(0, 0), 2) + pow(rectangle_coord.at(0,1), 2) + pow(rectangle_coord.at(0,2), 2));
};

Matrix<double> coordinate_system::get_sphere() noexcept {
	return sphere_coord;
}

Matrix<double> coordinate_system::get_rectange() const noexcept
{
	return rectangle_coord;
}

void coordinate_system::print(std::ostream& oss) noexcept{
	oss << "(" << this->sphere_coord.at(0, 0) << " " << this->sphere_coord.at(0,1) << " " << this->sphere_coord.at(0,2) << " )";
}

equatorial_system::equatorial_system(sky_system& sky_sys) noexcept: coordinate_system(sky_sys.get_sphere().at(0, 0), sky_sys.get_sphere().at(0, 1), sky_sys.get_sphere().at(0, 2)) {
	Time t;

	Matrix nut = _earth->update_nutation(t.time_TDB());
	Matrix pre = _earth->update_precission(t.time_TDB());
	Matrix M_np = nut.multiply(pre);

	this->sphere_coord = sky_sys.get_sphere().multiply(M_np);
}

sky_system::sky_system(ecliptic_system& ecl_sys, double time_TDB) noexcept: coordinate_system(ecl_sys.get_sphere().at(0,0), ecl_sys.get_sphere().at(0,1), ecl_sys.get_sphere().at(0,2)) {
	Time t;
	Matrix M_p = _earth->update_precission(time_TDB).transpose();
	Matrix rot_X = get_rot_x(-_earth->get_ε());
	Matrix R = M_p.multiply(rot_X);
	Matrix temp = ecl_sys.get_rectange().transpose();
	this->rectangle_coord = R.multiply(temp);
	this->rectangle2sphere();
}

sky_system::sky_system(equatorial_system& eq_sys) noexcept: coordinate_system(eq_sys.get_sphere().at(0, 0), eq_sys.get_sphere().at(0, 1), eq_sys.get_sphere().at(0, 2))
{
	Time t;

	Matrix nut = _earth->update_nutation(t.time_TDB());
	Matrix pre = _earth->update_precission(t.time_TDB());
	Matrix M_pn = nut.multiply(pre).transpose();

	this->sphere_coord = eq_sys.get_sphere().multiply(M_pn);
}

sky_system::sky_system(earth_system& earth_sys) noexcept: coordinate_system(earth_sys.get_sphere().at(0, 0), earth_sys.get_sphere().at(0, 1), earth_sys.get_sphere().at(0, 2))
{
	Time t;

	Matrix nut = _earth->update_nutation(t.time_TDB());
	Matrix pre = _earth->update_precission(t.time_TDB());

	Matrix R = nut.multiply(pre);

	Matrix M_tc = _earth->update_rotate_earth(t.time_Grinvich(), t.time_TDB()).multiply(R).transpose();

	this->sphere_coord = earth_sys.get_sphere().multiply(M_tc);
}

earth_system::earth_system(sky_system& sky_sys) noexcept: coordinate_system(sky_sys.get_sphere().at(0, 0), sky_sys.get_sphere().at(0, 1), sky_sys.get_sphere().at(0, 2))
{
	Time t;

	Matrix nut = _earth->update_nutation(t.time_TDB());
	Matrix pre = _earth->update_precission(t.time_TDB());

	Matrix R = nut.multiply(pre);

	Matrix M_ct = _earth->update_rotate_earth(t.time_Grinvich(), t.time_TDB()).multiply(R);

	this->sphere_coord = sky_sys.get_sphere().multiply(M_ct);
}

earth_system::earth_system(sky_system& sky_sys, double time_TDB) noexcept : coordinate_system(sky_sys.get_sphere().at(0, 0), sky_sys.get_sphere().at(0, 1), sky_sys.get_sphere().at(0, 2))
{
	Time t;

	Matrix nut = _earth->update_nutation(time_TDB);
	Matrix pre = _earth->update_precission(time_TDB);

	Matrix R = nut.multiply(pre);

	Matrix M_ct = _earth->update_rotate_earth(t.time_Grinvich(time_TDB), time_TDB).multiply(R);

	this->sphere_coord = sky_sys.get_sphere().multiply(M_ct);
	this->sphere2rectangle();
}

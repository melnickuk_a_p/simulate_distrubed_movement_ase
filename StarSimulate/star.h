#pragma once
#include "matrix.h"

class Star {
private:
	float mass_;
	Matrix r_, v_;
public:
	Star(std::vector<float> r, std::vector<float> v, float m);
};
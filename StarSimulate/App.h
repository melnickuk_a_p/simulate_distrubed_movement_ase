﻿#pragma once
#include "Window.h"
#include "Timer.h"
#include "ImGuiManager.h"
#include "Camera.h"
#include "Menu.h"
#include "matrix.hpp"
#include "PointLight.h"
#include <thread>
#include <mutex>
#include <condition_variable>

class App
{
public:
	App(int width, int height);

	int Go();
	~App();

	auto& get_queue() {
		return date;
	}

	bool SimulateEnabled() const noexcept {
		return start_simulate;
	}

	std::vector<std::unique_ptr<class Drawable>> drawables;
private:
	void DoFrame();
private:
	//For build window&graphics
	ImGuiManager imgui;
	Window wnd;
	
	//For build object
	Timer timer;
	float speed_factor = 1.0f;

	//For UI
	PointLight light;
	Camera cam;
	Menu menu;
	bool start_simulate = false;
	int Tab = 1;

	//For background
	ID3D11ShaderResourceView* pBack;

	//For simulate
	std::queue<std::tuple<Matrix<double>,Matrix<double>, double>> date;
	std::vector<double> kepler{ 0,0,0,0,0,0 };
	double a = 0.0, e = 0.0, i = 0.0, Ω =0.0, ω =0.0, M=0.0;
	int day = 0, month = 0, year = 0, hour = 0, minute = 0, second = 0;
};


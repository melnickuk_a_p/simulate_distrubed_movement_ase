﻿#include "time.h"

Time::Time()
{
	t = time(0);
	tm* gmtm = gmtime(&t);
	
	_year = gmtm->tm_year;
	_month = gmtm->tm_mon - 3 + 1; //Поправка на хз что
	if (_month < 0) {
		_month += 12;
		_year -= 1;
	}
	_day = gmtm->tm_mday;
	_hour = gmtm->tm_hour;
	_second = gmtm->tm_sec;
	_minute = gmtm->tm_min;
	
	//delete gmtm;
}

Time::Time(int year, int month, int day, int hour, int minute, int second): _day(day), _hour(hour), _minute(minute), _second(second)
{
	_year = year-2000;
	_month = month - 3 + 1; //Поправка на хз что
	if (_month < 0) {
		_month += 12;
		_year -= 1;
	}
}

double Time::time_MJD() noexcept {
	double mjd;

	mjd = 15078.0 + 365.0 * _year + floor(_year / 4.0) + floor(0.5 + 30.6 * _month);
	mjd += _day + (_hour) / 24.0 + (_minute) / 1440.0 + _second / 86400.0;

	return mjd;
}

double Time::time_TT() noexcept {
	double mjd = this->time_MJD();
	double delta_T = this->get_delta_T();
	return (mjd + delta_T / 86400.0);
}

double Time::get_delta_T(){
	return 69.184;
}

double Time::get_delta_UT()
{
	return -97.5E-3;
}

double Time::time_Grinvich() noexcept
{
	double delta_UT = get_delta_UT();
	double t_c = time_MJD() + delta_UT/ 86400.0;
	double T_U = (floor(t_c) - 51544.5) / 36525.0;
	double S_m_0_h = 1.753368559233266 + (628.3319706888409 + (6.770714E-6 - 4.51E-10 * T_U) * T_U) * T_U;
	double r = 6.300388098984891 + (3.707456E-10 - 3.707E-14 * T_U) * T_U;
	
	return S_m_0_h + r*(t_c-floor(t_c));
}

double Time::time_Grinvich(double time_TDB) noexcept
{
	double delta_UT = get_delta_UT();
	double t_c = time_MJD() + delta_UT / 86400.0;
	double T_U = (floor(t_c) - 51544.5) / 36525.0;
	double S_m_0_h = 1.753368559233266 + (628.3319706888409 + (6.770714E-6 - 4.51E-10 * T_U) * T_U) * T_U;
	double r = 6.300388098984891 + (3.707456E-10 - 3.707E-14 * T_U) * T_U;

	return S_m_0_h + r * (t_c - floor(t_c));
}

double Time::time_TDB() noexcept {
	double TT = this->time_TT();
	double d = (TT - 51544.5) / 36525.0;
	double g = 0.017453 * (357.258 + 35999.050 * d);
	return (TT + 0.001658 * sin(g + 0.0167 * sin(g)) / 86400.0);
}

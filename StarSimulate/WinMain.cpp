#include "App.h"

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine,
	_In_ int nCmdShow)
{
	try {
		return App{1000, 625}.Go();
	}
	catch (const my_exception& e) {
		MessageBox(nullptr, e.what(), e.GetType(), MB_OK | MB_ICONEXCLAMATION);
	}
	catch (const std::exception* e) {
		MessageBox(nullptr, e->what(), "Standart Exception", MB_OK | MB_ICONEXCLAMATION);
	}
	catch (...) {
		MessageBox(nullptr, "No detais available", "Uknown Exception", MB_OK | MB_ICONEXCLAMATION);
	}

	return -1;
}
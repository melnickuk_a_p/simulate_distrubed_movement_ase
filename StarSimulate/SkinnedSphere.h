#pragma once
#include "SkyObject.h"

class SkinnedSphere : public SkyObject<SkinnedSphere>
{
public:
	SkinnedSphere(Graphics& gfx, sky_system& position, const char* filenameTexture, float radius);
private:
	float radius;
};

#include "my_exception.h"
#include <sstream>

my_exception::my_exception(int line, const char* file) noexcept :
	line(line),
	file(file)
{};

const char* my_exception::what() const noexcept
{
	std::ostringstream oss;
	oss << GetType() << std::endl
		<< GetOriginString();
	whatBuffer = oss.str();

	return whatBuffer.c_str();
}

const char* my_exception::GetType() const noexcept
{
	return "My Exception";
}

int my_exception::GetLine() const noexcept
{
	return line;
}

const std::string& my_exception::GetFile() const noexcept
{
	return file;
}

std::string my_exception::GetOriginString() const noexcept
{
	std::ostringstream oss;
	oss << " [File] " << file << std::endl
		<< " [Line] " << line;
	return oss.str();
}

#pragma once

#include <iostream>
#include <iomanip>
#include <ctime>
#include <memory>
#include "sky_object.h"

class Time {
private:
	std::time_t t = std::time(nullptr);
	
	double get_delta_T();
	
	double get_delta_UT();

	int _year;
	int _month;
	int _day;
	int _hour;
	int _minute;
	int _second;
public:
	Time();

	Time(int year, int month, int day, int hour, int minute, int second);

	double time_MJD() noexcept;

	double time_TT() noexcept;

	double time_TDB() noexcept;

	double time_Grinvich() noexcept;

	double time_Grinvich(double time_TDB) noexcept;
};
﻿#pragma once

#include "matrix.hpp"
#include "sky_object.h"

namespace sky_object {
	class Earth;
}

class coordinate_system{
protected:
	Matrix<double> sphere_coord;
	Matrix<double> rectangle_coord;

	std::unique_ptr<sky_object::Earth> _earth;
public:
	coordinate_system() noexcept;

	//Принимает сферические координаты
	coordinate_system(double _α, double _β, double _r) noexcept;

	void sphere2rectangle() noexcept;

	void rectangle2sphere() noexcept;

	Matrix<double> get_sphere() noexcept;
	void set_sphere(Matrix<double> mat) noexcept {
		sphere_coord = mat;
	}

	Matrix<double> get_rectange() const noexcept;
	void set_rectange(Matrix<double> mat) noexcept {
		rectangle_coord = mat;
	}

	void print(std::ostream& oss) noexcept;

	friend std::ostream& operator<<(std::ostream& oss, coordinate_system& coord) {
		coord.print(oss);
		return oss;
	}
};

class sky_system; class earth_system; class ecliptic_system; class equatorial_system;

class ecliptic_system: public coordinate_system {
public:
	ecliptic_system() {};

	//Принимает сферические координаты
	ecliptic_system(double λ, double β, double r) noexcept: coordinate_system(λ, β, r) {};
};

class equatorial_system : public coordinate_system {
public:
	equatorial_system() {};

	//Принимает сферические координаты
	equatorial_system(double δ, double α, double r) noexcept: coordinate_system(δ, α, r) {};
	
	//Выдает в сферических координатах
	explicit equatorial_system(sky_system& sky_sys) noexcept;
};

class sky_system : public coordinate_system {
public:
	sky_system() {};

	//Принимает сферические координаты
	sky_system(double α, double β, double r) noexcept: coordinate_system(α, β, r) {};
	
	//Выдает в сферических координатах
	explicit sky_system(ecliptic_system& ecl_sys, double time_TDB) noexcept;

	//Выдает в сферических координатах
	explicit sky_system(equatorial_system& eq_sys) noexcept;

	//Выдает в сферических координатах
	explicit sky_system(earth_system& earth_sys) noexcept;

	sky_system operator-() {
		sky_system output{ 0,0,0 };
		Matrix<double> buff = rectangle_coord;
		output.set_rectange(Matrix<double>(3, 1, { -buff.at(0,0),-buff.at(0,1),-buff.at(0,2) }));
		rectangle2sphere();
		return output;
	}
	
	friend sky_system& operator+(sky_system& lval, sky_system& rval) {
		auto l = lval.get_rectange();
		auto r = rval.get_rectange();
		lval.set_rectange(l + r);
		lval.rectangle2sphere();

		return lval;
	}

};

class earth_system : public coordinate_system {
public:
	earth_system() {};

	//Принимает сферические координаты
	earth_system(double α, double β, double r) noexcept: coordinate_system(α, β, r) {};

	//Выдает в сферических координатах
	explicit earth_system(sky_system& sky_sys) noexcept;

	//Выдает в сферических координатах
	explicit earth_system(sky_system& sky_sys, double time_TDB) noexcept;
};

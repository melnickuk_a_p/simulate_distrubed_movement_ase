﻿#include "sky_object.h"

#define _USE_MATH_DEFINES
#include <math.h>

#define NORMALIZE_RADIAN(angle) while(angle>2*M_PI){angle-=2*M_PI;}

namespace sky_object {

	Fundamental_var Fundamental_var::operator() (double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;

		l_ = r_g * (357.52772333 + (35999.05034 - (0.00016028 + 0.00000333 * temp) * temp) * temp);
		D = r_g * (297.85036306 + (445267.11148 - (0.00191417 - 0.00000528 * temp) * temp) * temp);
		λ = r_g * (218.31643250 + (481267.8812772222 - (0.00161167 - 0.00000528 * temp) * temp) * temp);
		l = r_g * (134.96298139 + (477198.8673980556 + (0.00869722 + 0.00001778 * temp) * temp) * temp);
		F = r_g * (93.27191028 + (483202.0175380555 - (0.00368250 - 0.00000306 * temp) * temp) * temp);

		NORMALIZE_RADIAN(l);
		NORMALIZE_RADIAN(l_);
		NORMALIZE_RADIAN(D);
		NORMALIZE_RADIAN(F);
		NORMALIZE_RADIAN(λ);

		return { l_ , D, λ , l , F };
	}

	ecliptic_system Sun::coord_Sun(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;
		double AU = 149597870.691;
		double r_s = 4.848136811095E-6;
		double u_3 = 6.23999846 + 628.30194562 * temp;
		double D = 5.19870752 + 7771.37722506 * temp;
		double δL_s = 6892.76 * sin(u_3) + 71.98 * sin(2 * u_3);
		double δR_s = (-16707.4 + 42.0 * temp) * cos(u_3) - 139.57 * cos(2 * u_3) + 30.76 * cos(D);

		L_s = 4.93823996 + u_3 + r_s * (6191.2 * temp + δL_s);
		B_s = 0;
		R_s = AU * (1.0001398 + 1.0E-6 * δR_s);

		NORMALIZE_RADIAN(L_s);

		return { L_s, B_s, R_s };
	};

	ecliptic_system Moon::coord_Moon(double time_TDB) {
		Fundamental_var var;
		var = var(time_TDB);

		double sin_pi = 3422.70 + 28.233869 * cos(2 * var.D) + 3.08589 * cos(var.l + 2 * var.D)
			+ 186.539296 * cos(var.l) + 34.311569 * cos(var.l - 2 * var.D)
			+ 1.916735 * cos(var.l_ - 2 * var.D) - 0.977818 * cos(var.D)
			+ 10.165933 * cos(2 * var.l) - 0.949147 * cos(var.l + var.l_)
			+ 1.443617 * cos(var.l + var.l_ - 2 * var.D);

		double δλ_c = 22640 * sin(var.l) - 4586 * sin(var.l - 2 * var.D)
			+ 2370 * sin(2 * var.D) + 769 * sin(2 * var.l)
			- 668 * sin(var.l_) - 412 * sin(2 * var.F)
			- 212 * sin(2 * var.l - 2 * var.D) - 206 * sin(var.l + var.l_ - 2 * var.D)
			+ 192 * sin(var.l + 2 * var.D) - 165 * sin(var.l_ - 2 * var.D)
			- 125 * sin(var.D) - 110 * sin(var.l + var.l_) + 148 * sin(var.l - var.l_) - 55 * sin(2 * var.F - 2 * var.D);

		double r_s = 4.8481366811095E-6;

		double δS = var.F + r_s * (δλ_c + 412 * sin(2 * var.F) + 541 * sin(var.l));

		double δN = -526 * sin(var.F - 2 * var.D)
			+ 44 * sin(var.l + var.F - 2 * var.D) - 31 * sin(-var.l + var.F - 2 * var.D)
			- 23 * sin(var.l_ + var.F - 2 * var.D) + 11 * sin(-var.l_ + var.F - 2 * var.D)
			- 25 * sin(-2 * var.l + var.F) + 21 * sin(-var.l + var.F);

		double β_c = 18520.0 * sin(δS) + δN;

		this->λ_M = var.λ + r_s * δλ_c;
		this->β_M = r_s * β_c;
		this->R_M = 6378.14 / (0.999953253 * r_s * sin_pi);

		return { λ_M, β_M, R_M };
	}

	double Earth::get_time_Star(double time_Grinvich, double time_TDB) {
		update_Δψ(time_TDB);
		update_ε(time_TDB);

		return time_Grinvich + Δψ * cos(ε);
	}

	Earth::Earth() : r_s(4.848136811095E-6) {};

	void Earth::update_ε(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;
		this->ε = r_s * (84381.448 - (46.815 + (0.0059 - 0.001813 * temp) * temp) * temp);
	}

	void Earth::update_param_precession(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;

		this->ζ = r_s * (2306.2181 + (0.30188 + 0.017998 * temp) * temp) * temp;
		this->θ = r_s * (2004.3109 - (0.42665 + 0.041833 * temp) * temp) * temp;
		this->z = r_s * (2306.2181 + (1.09468 + 0.018203 * temp) * temp) * temp;
	}

	void Earth::update_Δψ(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;

		Fundamental_var var;
		var = var(time_TDB);

		double λ = var.λ;
		double F = var.F;
		double l = var.l;
		double l_ = var.l_;
		double D = var.D;

		this->Δψ = r_s * ((-17.1996 - 0.01742 * temp) * sin(λ - F) + (0.2062 + 0.00002 * temp) * sin(2 * λ - 2 * F) +
			0.0046 * sin(λ - 2 * l + F) + 0.0011 * sin(2 * l - 2 * F) -
			(1.3187 + 0.00016 * temp) * sin(2 * λ - 2 * D) + (0.1426 - 0.00034 * temp) * sin(l_) -
			(0.0517 - 0.00012 * temp) * sin(2 * λ + l_ - 2 * D) + (0.0217 - 0.00005 * temp) * sin(2 * λ - l_ - 2 * D) +
			(0.0129 + 0.00001 * temp) * sin(λ + F - 2 * D) + 0.0048 * sin(2 * l - 2 * D) -
			0.0022 * sin(2 * F - 2 * D) + (0.0017 - 0.00001 * temp) * sin(2 * l_) -
			(0.0016 - 0.00001 * temp) * sin(2 * λ + 2 * l_ - 2 * D) - 0.0015 * sin(λ + l_ - F) -
			0.0012 * sin(λ - l_ - F));

		this->Δψ += r_s * ((-0.2274 - 0.00002 * temp) * sin(2 * λ) + (0.0712 + 0.00001 * temp) * sin(l) -
			(0.0386 + 0.00004 * temp) * sin(λ + F) - 0.0301 * sin(2 * λ + l) -
			0.0158 * sin(l - 2 * D) + 0.0123 * sin(2 * λ - l) + 0.0063 * sin(2 * D) +
			(0.0063 + 0.00001 * temp) * sin(λ + l - F) - (0.0058 + 0.00001 * temp) * sin(λ - l - F) -
			0.0059 * sin(2 * λ - l + 2 * D) - 0.0051 * sin(λ + l + F) -
			0.0038 * sin(2 * λ + 2 * D) + 0.0029 * sin(2 * l) + 0.0029 * sin(2 * λ + l - 2 * D) -
			0.0031 * sin(2 * λ + 2 * l) + 0.0026 * sin(2 * F) + 0.0021 * sin(λ - l + F) +
			0.0016 * sin(λ - l - F + 2 * D) - 0.0013 * sin(λ + l - F - 2 * D) -
			0.0010 * sin(λ - l + F + 2 * D));

	}

	void Earth::update_Δε(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;

		Fundamental_var var;
		var = var(time_TDB);

		double λ = var.λ;
		double F = var.F;
		double l = var.l;
		double l_ = var.l_;
		double D = var.D;

		this->Δε = r_s * ((9.2025 + 0.00089 * temp) * cos(λ - F) - (0.0895 - 0.00005 * temp) * cos(2 * λ - 2 * F) -
			0.0024 * cos(λ - 2 * l + F) + (0.5736 - 0.00031 * temp) * cos(2 * λ - 2 * D) +
			(0.0054 - 0.00001 * temp) * cos(l_) + (0.0224 - 0.00006 * temp) * cos(2 * λ + l_ - 2 * D) -
			(0.0095 - 0.00003 * temp) * cos(2 * λ - l_ - 2 * D) - 0.0070 * cos(λ + F - 2 * D));

		this->Δε += r_s * ((0.0977 - 0.00005 * temp) * cos(2 * λ) + 0.02 * cos(λ + F) +
			(0.0129 - 0.00001 * temp) * cos(2 * λ + l) - 0.0053 * cos(2 * λ - l) -
			0.0033 * cos(λ + l - F) + 0.0032 * cos(λ - l - F) +
			0.0026 * cos(2 * λ - l + 2 * D) + 0.0027 * cos(λ + l + F) + 0.0016 * cos(2 * λ + 2 * D) -
			0.0012 * cos(2 * λ + l - 2 * D) + 0.0013 * cos(2 * λ + 2 * l) - 0.001 * cos(λ - l + F));
	}

	Matrix<double> Earth::update_precission(double time_TDB) {

		update_param_precession(time_TDB);
		update_ε(time_TDB);

		Matrix temp_matrix = get_rot_z(-ζ);
		precission = get_rot_y(θ).multiply(temp_matrix);
		precission = get_rot_z(-z).multiply(precission);

		return precission;
	}

	Matrix<double> Earth::update_nutation(double time_TDB) {
		double temp = (time_TDB - 51544.5) / 36525.0;

		update_ε(time_TDB);
		update_Δψ(time_TDB);
		update_Δε(time_TDB);

		Matrix temp_matrix = get_rot_x(this->ε);
		nutation = get_rot_z(-Δψ).multiply(temp_matrix);
		nutation = get_rot_x(-ε - Δε).multiply(nutation);

		return nutation;
	}

	Matrix<double> Earth::update_rotate_earth(double time_Grinvich, double time_TDB) {
		double star_Time = get_time_Star(time_Grinvich, time_TDB);

		Rotate = get_rot_z(star_Time);

		return Rotate;
	}

	double Earth::get_Δψ() const {
		return this->Δψ;
	}

	double Earth::get_ε() const {
		return this->ε;
	}
}
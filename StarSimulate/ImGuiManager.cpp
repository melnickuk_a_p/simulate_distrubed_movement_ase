#include "ImGuiManager.h"
#include "imgui/imgui.h"
#include "imgui/implot.h"

ImGuiManager::ImGuiManager() {
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGui::StyleColorsDark();

	ImGui::GetIO().Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
}

ImGuiManager::~ImGuiManager() {
	ImPlot::DestroyContext();
	ImGui::DestroyContext();
}
#pragma once
#include "DrawableBase.h"
#include "coordinate_system.h"

template<class T>
class SkyObject : public DrawableBase<T>
{
public:
	SkyObject(Graphics& gfx, sky_system& position)
	{
		earth_system tmp_sys = earth_system{ position, 50000.0 };
		Matrix<double> tmp = tmp_sys.get_rectange();
		tmp = tmp/64000.0;
		pos.set_rectange(tmp);
		pos.rectangle2sphere();
	};

	void Update(float dt) noexcept override {
		yaw += dyaw*dt;
	}

	void UpdateFromCoord(sky_system& coord, double time_TDB) noexcept override
	{
		pitch = time_TDB;
		Matrix<double> tmp = coord.get_rectange();
		tmp = tmp / 64000.0;
		pos.set_rectange(tmp);
		pos.rectangle2sphere();
	}

	DirectX::XMMATRIX GetTransformXM() const noexcept
	{
		Matrix<double> r = pos.get_rectange();
		namespace dx = DirectX;
		return dx::XMMatrixRotationRollPitchYaw(0.0f, yaw, pitch) *
			dx::XMMatrixTranslation(r.at(0,0), r.at(0,2), r.at(0,1));
	}

private:
	// positional
	earth_system pos{0,0,0};

	float pitch = 0.0f;
	float yaw=0.0;
	float dyaw = 1.0f;
};
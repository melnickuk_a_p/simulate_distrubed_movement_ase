﻿#include "Sattelite.h"
#include <iostream>

void Sattelite::set_Kepler(std::vector<double> cart)
{
    double x = cart.at(0);
    double y = cart.at(1);
    double z = cart.at(2);
    double dx = cart.at(3);
    double dy = cart.at(4);
    double dz = cart.at(5);

    double r = sqrt(x * x + y * y + z * z);
    double V = sqrt(dx * dx + dy * dy + dz * dz);

    double h = V * V / 2 - fm_ / r;

    double c_1 = y * dz - z * dy;
    double c_2 = z * dx - x * dz;
    double c_3 = x * dy - y * dx;

    double l_1 = -fm_ * x / r + dy * c_3 - dz * c_2;
    double l_2 = -fm_ * y / r + dz * c_1 - dx * c_3;
    double l_3 = -fm_ * z / r + dx * c_2 - dy * c_1;

    double c = sqrt(c_1 * c_1 + c_2 * c_2 + c_3 * c_3);
    double l = sqrt(l_1 * l_1 + l_2 * l_2 + l_3 * l_3);

    m_kepler.a = -fm_ / (2 * h);
    m_kepler.e = l / fm_;
    
    double p = c * c / fm_;

    double cos_i = c_3 / c;
    double sin_i = sqrt(1 - pow(cos_i, 2));
    m_kepler.i = atan2(sin_i,cos_i);

    double cos_Ω = -c_2 / (c * sin_i);
    double sin_Ω = c_1 / (c * sin_i);
    m_kepler.Ω = atan2(sin_Ω , cos_Ω);

    double sin_ω = l_3 / (l * sin_i);;
    double cos_ω = l_1 / l * cos_Ω + l_2 / l * sin_Ω;
    m_kepler.ω = atan2(sin_ω,cos_ω);
    
    double sin_u = z / (r * sin_i);
    double cos_u = x / r * cos_Ω + y / r * sin_Ω;
    double sin_v = sin_u * cos_ω - cos_u * sin_ω;
    double cos_v = cos_u * cos_ω + sin_u * sin_ω;
    double sin_E = sqrt(1 - pow(m_kepler.e, 2)) * sin_v / (1 + m_kepler.e * cos_v);
    double cos_E = (cos_v + m_kepler.e) / (1 + m_kepler.e * cos_v);
    
    double E = atan2(sin_v,cos_v) + atan2((sin_E * cos_v - cos_E * sin_v), (cos_E * cos_v + sin_E * sin_v));
    m_kepler.M = E - m_kepler.e * sin(E);
}

std::vector<double> Sattelite::kepler_2_cart()
{
    double x, y, z, dx, dy, dz;

    double E = m_kepler.M;
    for(uint32_t count = 0 ; count < 15; ++count)
        E -= (E - m_kepler.e * sin(E) - m_kepler.M) / (1-m_kepler.e*cos(E));

    double sin_v = sqrt(1 - pow(m_kepler.e, 2)) * sin(E) / (1 - m_kepler.e * cos(E));
    double cos_v = (cos(E) - m_kepler.e) / (1 - m_kepler.e * cos(E));
    double sin_u = sin_v * cos(m_kepler.ω) + cos_v * sin(m_kepler.ω);
    double cos_u = cos_v * cos(m_kepler.ω) - sin_v * sin(m_kepler.ω);

    double r = m_kepler.a * (1 - m_kepler.e * cos(E));
    double p = m_kepler.a*(1 - pow(m_kepler.e, 2));
    double V_r = sqrt(fm_ / p) * m_kepler.e * sin_v;
    double V_n = sqrt(fm_ / p) * (1 + m_kepler.e * cos_v);

    x = r * (cos_u * cos(m_kepler.Ω) - sin_u * sin(m_kepler.Ω) * cos(m_kepler.i));
    y = r * (cos_u * sin(m_kepler.Ω) + sin_u * cos(m_kepler.Ω) * cos(m_kepler.i));
    z = r * sin_u * sin(m_kepler.i);

    dx = x / r * V_r + (-sin_u * cos(m_kepler.Ω) - cos_u * sin(m_kepler.Ω) * cos(m_kepler.i)) * V_n;
    dy = y / r * V_r + (-sin_u * sin(m_kepler.Ω) + cos_u * cos(m_kepler.Ω) * cos(m_kepler.i)) * V_n;
    dz = z / r * V_r + cos_u * sin(m_kepler.i) * V_n;

    return { x,y,z,dx,dy,dz };
}

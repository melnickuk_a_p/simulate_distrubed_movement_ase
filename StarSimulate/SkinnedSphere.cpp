#include "SkinnedSphere.h"
#include "BindableBase.h"
#include "Sphere.h"
#include "Sampler.h"
#include "Texture.h"
#include "Cube.h"
#include "Surface.h"

SkinnedSphere::SkinnedSphere(Graphics& gfx, sky_system& position, const char* filenameTexture,float radius):
	SkyObject(gfx, position), radius(radius)
{
	namespace dx = DirectX;

	if (!IsStaticInitialized()) {

		AddStaticBind(std::make_unique<Sampler>(gfx));

		auto pvs = std::make_unique<VertexShader>(gfx, L"TexturePhongVS.cso");
		auto pvsbc = pvs->GetBytecode();
		AddStaticBind(std::move(pvs));

		AddStaticBind(std::make_unique<PixelShader>(gfx, L"TexturePhongPS.cso"));

		const std::vector<D3D11_INPUT_ELEMENT_DESC> ied = {
			{"Position",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0},
			{ "Normal",0,DXGI_FORMAT_R32G32B32_FLOAT,0,12,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TexCoord",0,DXGI_FORMAT_R32G32_FLOAT,0,24,D3D11_INPUT_PER_VERTEX_DATA,0 },
		};
		AddStaticBind(std::make_unique<InputLayout>(gfx, ied, pvsbc));
		AddStaticBind(std::make_unique<Topology>(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));

		struct PSMaterialConstants
		{
			float specularIntensity = 0.6f;
			float specularPower = 30.0f;
			float padding[2];
		} colorConst;
		AddStaticBind(std::make_unique<PixelConstantBuffer<PSMaterialConstants>>(gfx, colorConst, 1u));
	}

	struct Vertex {
		dx::XMFLOAT3 pos;
		dx::XMFLOAT3 n;
		dx::XMFLOAT2 tc;
	};

	//auto model = Sphere::Make<Vertex>();
	auto model = Sphere::Make<Vertex>();
	model.Transform(DirectX::XMMatrixScaling(radius, radius, radius));
	model.SetNormalWithTextureFlat();

	//model.Transform(dx::XMMatrixScaling(radius, radius, radius));
	AddBind(std::make_unique<VertexBuffer>(gfx, model.vertices));

	//AddStaticIndexBuffer(std::make_unique<IndexBuffer>(gfx, model.indices));
	AddIndexBuffer(std::make_unique<IndexBuffer>(gfx, model.indices));

	AddBind(std::make_unique<Texture>(gfx, Surface::FromFile(filenameTexture)));

	AddBind(std::make_unique<TransformCbuf>(gfx, *this));

}

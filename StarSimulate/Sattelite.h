﻿#pragma once
#include <vector>
#include "coordinate_system.h"

struct Kepler {
	double a;
	double e;
	double i;
	double Ω;
	double ω;
	double M;

	double at(uint32_t pos) {
		switch (pos) {
			case 0:
				return a;
				break;
			case 1:
				return e;
				break;
			case 2:
				return i;
				break;
			case 3:
				return Ω;
				break;
			case 4:
				return ω;
				break;
			case 5:
				return M;
				break;

		}
	}
};

class Sattelite
{
private:
	Kepler m_kepler;
	sky_system r;
	sky_system velocity;

	const double fm_ = 398600.44150;
	double mass;
	double S;
public:
	Sattelite(Kepler kepler) noexcept: m_kepler(kepler) {
		std::vector<double> tmp = kepler_2_cart();
		r = sky_system{ 0,0,0 };
		velocity = sky_system{ 0,0,0 };
		r.set_rectange(Matrix<double>(3, 1, { tmp.at(0), tmp.at(1), tmp.at(2) }));
		velocity.set_rectange(Matrix<double>(3, 1, { tmp.at(3), tmp.at(4), tmp.at(5) }));
		r.rectangle2sphere();
		velocity.rectangle2sphere();
	};
	Sattelite(std::vector<double> vec)noexcept {
		set_Kepler(vec);
		r.set_rectange(Matrix<double>(3, 1, { vec.at(0), vec.at(1), vec.at(2) }));
		velocity.set_rectange(Matrix<double>(3, 1, { vec.at(3), vec.at(4), vec.at(5) }));
	}
	Sattelite(Sattelite& sat) = delete;
	Sattelite& operator=(Sattelite& sat) = delete;

	void set_Kepler(std::vector<double> cart);
	void set_Kepler(Kepler& kep) {
		m_kepler = kep;
		std::vector<double> tmp = kepler_2_cart();
		r = sky_system{ 0,0,0 };
		velocity = sky_system{ 0,0,0 };
		r.set_rectange(Matrix<double>(3, 1, { tmp.at(0), tmp.at(1), tmp.at(2) }));
		velocity.set_rectange(Matrix<double>(3, 1, { tmp.at(3), tmp.at(4), tmp.at(5) }));
		r.rectangle2sphere();
		velocity.rectangle2sphere();
	}
	std::vector<double> kepler_2_cart();

	sky_system& get_r() { return r; };
	sky_system& get_velocity() { return velocity; };
	Kepler& get_kepler() { return m_kepler; };
};


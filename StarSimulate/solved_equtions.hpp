﻿#pragma once

#include <utility>
#include "matrix.hpp"
#include <algorithm>

class Solver {
private:
	Matrix<double> h;
	Matrix<double> c;
	Matrix<Matrix<double>> A;
	Matrix<Matrix<double>> α;

	template<typename T>
	T calculate_x_i(double t_i, T F_1, T x_1, T _x_1) {
		return (x_1 + _x_1 * t_i + (1.0 / 2.0) * F_1 * pow(t_i, 2) + (1.0 / 6.0) * A(0, 0) * pow(t_i, 3) + (1.0 / 12.0) * A(0, 1) * pow(t_i, 4) +
			(1.0 / 20.0) * A(0, 2) * pow(t_i, 5) + (1.0 / 30.0) * A(0, 3) * pow(t_i, 6) + (1.0 / 42.0) * A(0, 4) * pow(t_i, 7) +
			(1.0 / 56.0) * A(0, 5) * pow(t_i, 8) + (1.0 / 72.0) * A(0, 6) * pow(t_i, 9));
	};

	template<typename T>
	T calculate__x_i(double t_i, T F_1, T _x_1) {
		return (_x_1 + F_1 * t_i + (1.0 / 2.0) * A(0, 0) * pow(t_i, 2) +
			(1.0 / 3.0) * A(0, 1) * pow(t_i, 3) + (1.0 / 4.0) * A(0, 2) * pow(t_i, 4) +
			(1.0 / 5.0) * A(0, 3) * pow(t_i, 5) + (1.0 / 6.0) * A(0, 4) * pow(t_i, 6) +
			(1.0 / 7.0) * A(0, 5) * pow(t_i, 7) + (1.0 / 8.0) * A(0, 6) * pow(t_i, 8));
	}

	//Требуется i = 1..7
	void update_A() {
		for (uint32_t i = 0; i < 7; ++i) {
			A(0, i) = Matrix<double>(3,1,{0,0,0});
			for (uint32_t j = i; j < 7; ++j) {
				A(0, i) += c(j, i) * α(0, j);
			}
		}
	};

public:
	Solver() :
		h(1, 8, { 0.0,
				0.056262560526922147,
				0.180240691736892365,
				0.352624717113169637,
				0.547153626330555383,
				0.734210177215410532,
				0.885320946839095768,
				0.977520613561287501 }),
		c(7, 7),
		A(1, 7, { {3,1}, {3,1}, {3,1}, {3,1}, {3,1}, {3,1}, {3,1} }),
		α(1, 7, { {3,1}, {3,1}, {3,1}, {3,1}, {3,1}, {3,1}, {3,1} })
	{};

	template<typename func, typename T>
	std::tuple<T, T, double> method_Everharts(double t_1, double t_2, T r_1, T v_1, double step, func fnc) {

		int N = (t_2 - t_1) / (step/86400.0);

		//Матрица времени t_i
		Matrix<double> t(1, 8);
		for (uint32_t count = 0; count < 8; ++count) {
			t(0, count) = h(0, count) * step;
		}

		//Матрица с_ij
		for (uint32_t i = 0; i < 7; ++i) {
			c(i, i) = 1;
		}

		for (uint32_t i = 1; i < 7; ++i) {
			c(i, 0) = -t(0, i) * c(i - 1, 0);
			for (uint32_t j = 1; j < i; ++j) {
				c(i, j) = c(i - 1, j - 1) - t(0, i) * c(i - 1, j);
			}
		}

		T x_9;
		T _x_9;

		for (uint32_t count=0; count <N;++count) {

			//0 шаг
			T F_1 = fnc(r_1, t(0,0)+t_1);

			//Первый шаг
			T x_2 = calculate_x_i(t(0, 1), F_1, r_1, v_1);
			T F_2 = fnc(x_2, t(0, 1)/86400.0+t_1);
			α(0, 0) = (F_2 - F_1) / t(0, 1);
			update_A();

			//Второй шаг
			T x_3 = calculate_x_i(t(0, 2), F_1, r_1, v_1);
			T F_3 = fnc(x_3, t(0, 2)/86400.0 + t_1);
			α(0, 1) = ((F_3 - F_1) / t(0, 2) - α(0, 0)) / (t(0, 2) - t(0, 1));
			update_A();

			//Третий шаг
			T x_4 = calculate_x_i(t(0, 3), F_1, r_1, v_1);
			T F_4 = fnc(x_4, t(0, 3)/86400.0 + t_1);
			α(0, 2) = (((F_4 - F_1) / t(0, 3) - α(0, 0)) / (t(0, 3) - t(0, 1)) - α(0, 1)) / (t(0, 3) - t(0, 2));
			update_A();

			//Четвертый шаг
			T x_5 = calculate_x_i(t(0, 4), F_1, r_1, v_1);
			T F_5 = fnc(x_5, t(0, 4)/86400.0 + t_1);
			α(0, 3) = ((((F_5 - F_1) / t(0, 4) - α(0, 0)) / (t(0, 4) - t(0, 1)) - α(0, 1)) / (t(0, 4) - t(0, 2)) - α(0, 2)) / (t(0, 4) - t(0, 3));
			update_A();

			//Пятый шаг
			T x_6 = calculate_x_i(t(0, 5), F_1, r_1, v_1);
			T F_6 = fnc(x_6, t(0, 5)/86400.0 + t_1);
			α(0, 4) = (((((F_6 - F_1) / t(0, 5) - α(0, 0)) / (t(0, 5) - t(0, 1)) - α(0, 1)) / (t(0, 5) - t(0, 2)) - α(0, 2)) / (t(0, 5) - t(0, 3)) - α(0, 3)) / (t(0, 5) - t(0, 4));
			update_A();

			T x_7 = calculate_x_i(t(0, 6), F_1, r_1, v_1);
			T F_7 = fnc(x_7, t(0, 6)/86400.0 + t_1);
			α(0, 5) = ((((((F_7 - F_1) / t(0, 6) - α(0, 0)) / (t(0, 6) - t(0, 1)) - α(0, 1)) / (t(0, 6) - t(0, 2)) - α(0, 2)) / (t(0, 6) - t(0, 3)) - α(0, 3)) / (t(0, 6) - t(0, 4)) - α(0, 4)) / (t(0, 6) - t(0, 5));
			update_A();

			T x_8 = calculate_x_i(t(0, 7), F_1, r_1, v_1);
			T F_8 = fnc(x_8, t(0, 7)/86400.0 + t_1);
			α(0, 6) = (((((((F_8 - F_1) / t(0, 7) - α(0, 0)) / (t(0, 7) - t(0, 1)) - α(0, 1)) / (t(0, 7) - t(0, 2)) - α(0, 2)) / (t(0, 7) - t(0, 3)) - α(0, 3)) / (t(0, 7) - t(0, 4)) - α(0, 4)) / (t(0, 7) - t(0, 5)) - α(0, 5)) / (t(0, 7) - t(0, 6));
			update_A();

			x_9 = calculate_x_i(step, F_1, r_1, v_1);
			_x_9 = calculate__x_i(step, F_1, v_1);

			r_1 = x_9;
			v_1 = _x_9;
			t_1 += step/86000.0;
			
			for (uint32_t i = 0; i < 7; ++i)
				α(0, i) = T{3,1};
			
			update_A();
		}

		return std::make_tuple(r_1, v_1, t_1);
	}

	template<typename Func1, typename Func2>
	std::pair<std::vector<std::vector<double>>, std::vector<std::vector<double>>> runge4(double t_1, double t_2, std::vector<double> r_1, std::vector<double> v_1, double step, Func1 func1, Func2 func2) {
		std::vector < std::vector < double >> out_x;
		std::vector<std::vector<double>> out_dx;
			
		double x_1 = r_1.at(0);
		double y_1 = r_1.at(1);

		double vx = v_1.at(0);
		double vy = v_1.at(1);

		while (t_1 < t_2){
			t_1 += step;
			
			double q_0 = func1(x_1, y_1, t_1);
			double m_0 = func2(x_1, y_1, t_1);
			double k_0 = vx;
			double l_0 = vy;

			double q_1 = func1(x_1 + k_0 * step / 2.0, y_1+l_0*step/2.0, t_1+step/2.0);
			double m_1 = func2(x_1 + k_0 * step / 2.0, y_1 + l_0 * step / 2.0, t_1 + step / 2.0);
			double k_1 = vx + q_0 * step / 2.0;
			double l_1 = vy + m_0 * step / 2.0;

			double q_2 = func1(x_1 + k_1 * step / 2.0,y_1+l_1*step/2.0, t_1 + step / 2.0);
			double m_2 = func2(x_1 + k_1 * step / 2.0, y_1 + l_1 * step / 2.0, t_1 + step / 2.0);
			double k_2 = vx + q_1 * step / 2.0;
			double l_2 = vy + m_1 * step / 2.0;

			double q_3 = func1(x_1 + k_2 * step,y_1+l_2*step, t_1 + step);
			double m_3 = func2(x_1 + k_2 * step, y_1 + l_2 * step, t_1 + step);
			double k_3 = vx + q_2 * step;
			double l_3 = vy + m_2 * step;

			vx += step / 6.0 * (q_0 + 2 * q_1 + 2 * q_2 + q_3);
			x_1 += step / 6.0 * (k_0 + 2 * k_1 + 2 * k_2 + k_3);
			
			vy += step / 6.0 * (m_0 + 2 * m_1 + 2 * m_2 + m_3);
			y_1 += step / 6.0 * (l_0 + 2 * l_1 + 2 * l_2 + l_3);

			out_x.push_back({x_1,y_1});
			out_dx.push_back({vx, vy});
		}

		return { out_x, out_dx };
	}
};
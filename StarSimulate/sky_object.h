﻿#pragma once
#include "time.h"
#include "matrix.hpp"
#include "coordinate_system.h"
#include <memory>

class ecliptic_system;

namespace sky_object {

	struct Fundamental_var {
		double l_;
		double D;
		double λ;
		double l;
		double F;
		double r_g = 0.017453292519943296;

		Fundamental_var operator() (double time_TDB);
	};

	class Sun {
	private:	
		double L_s;
		double B_s;
		double R_s;
	public:
		Sun() {};

		ecliptic_system coord_Sun(double time_TDB);
	};

	class Moon {
	private:	
		double λ_M; 
		double β_M;
		double R_M;
	public:
		Moon() {};

		ecliptic_system coord_Moon(double time_TDB);
	};

	class Earth {
	protected:
		double ζ, θ, z, ε;		Matrix<double> precission;
		double Δψ, Δε, r_s;		Matrix<double> nutation;
								Matrix<double> Rotate;

		double get_time_Star(double time_Grinvich, double time_TDB);
	
		void update_ε(double time_TDB);

		void update_param_precession(double time_TDB);

		void update_Δψ(double time_TDB);

		void update_Δε(double time_TDB);
	public:
		Earth();

		Matrix<double> update_precission(double time_TDB);

		Matrix<double> update_nutation(double time_TDB);

		Matrix<double> update_rotate_earth(double time_Grinvich, double time_TDB);

		double get_Δψ() const;

		double get_ε() const;

		double get_Δε() const noexcept {
			return this->Δε;
		}

		std::vector<double> get_precission() noexcept {
			return { this->ζ, this->θ, this->z };
		}
	};
}


﻿#include "App.h"
#include "Box.h"
#include "Piramide.h"
#include <memory>
#include <algorithm>
#include "high_math.h"
#include "Surface.h"
#include "GDIPlusManager.h"
#include "imgui/imgui.h"
#include "imgui/implot.h"
#include "solved_equtions.hpp"
#include "Effect.h"
#include "Sattelite.h"
#include "acceleration.h"
#include "Cylinder.h"
#include "SkinnedCube.h"
#include "SkinnedSphere.h"

std::mutex m;

GDIPlusManager gdipm;
void simulate_thread(int year, int month, int day, int hour, int minute, int second, Sattelite& sat, App& app);

App::App(int width, int height) : wnd(width, height, "SAE"), menu(), light(wnd.Gfx())
{

	sky_system coord{ 0,0,0 };

	drawables.push_back(std::make_unique<SkinnedSphere>(wnd.Gfx(), coord, "..\\Images\\earth.png", 1.0f));
	drawables.push_back(std::make_unique<SkinnedSphere>(wnd.Gfx(), coord, "..\\Images\\moon.jpg", 1.0/2.0f));
	drawables.push_back(std::make_unique<SkinnedSphere>(wnd.Gfx(), coord, "..\\Images\\sat.jpg",1.0f/4.0f));

	wnd.Gfx().SetProjection(DirectX::XMMatrixPerspectiveLH(1.0f, 3.0f/4.0f, 0.5f, 100.0f));

	int out_width, out_height;
	wnd.Gfx().LoadBackgroundFromFile("..\\Images\\back2.jpg", &pBack, &out_width, &out_height);
}

void App::DoFrame()
{
	static Sattelite sat{ std::vector<double>{0,1,2,3,4,5} };
	static std::vector<Matrix<double>> r_vector;
	static std::vector<Matrix<double>> v_vector;
	static std::vector<double> t_vector;

	const auto dt = timer.Mark() * speed_factor;

	wnd.Gfx().SetRenderTarget();

	wnd.Gfx().BeginFrame(0.0f, 0.0f, 0.0f);

	menu.ThemeMenu();

	ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
	windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
	windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

	ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->Pos);
	ImGui::SetNextWindowSize(viewport->Size);
	ImGui::SetNextWindowViewport(viewport->ID);
	ImGui::SetNextWindowBgAlpha(0.0f);

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

	ImGui::Begin("InvisibleWindow", nullptr, windowFlags);
	ImGui::PopStyleVar(3);

	if (ImGui::BeginMenuBar()) {
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("Load File...")) {

			}
			if (ImGui::MenuItem("Save File...")) {

			}
			if (ImGui::MenuItem("Save File As ...")) {

			}
			if (ImGui::MenuItem("Exit")) {
				PostQuitMessage(0);
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Example")) {
			if (ImGui::MenuItem("#1")) {
				a = 320000.0;
				e = 0.01203;
				i = 5.0;
				Ω = 95.997;
				ω = 145.2777;
				M = 170.03184;
				day = 15;
				month = 8;
				year = 2022;
				hour = 4;
				minute = 11;
				second = 45;
			}
			if (ImGui::MenuItem("#2")) {
				a = 42110.615261;
				e = 0.03;
				i = 1.2;
				Ω = 172.1;
				ω = -145.0;
				M = 150.0;
				day = 17;
				month = 11;
				year = 2000;
				hour = 1;
				minute = 1;
				second = 1;
			}
			if (ImGui::MenuItem("#3")) {
				a = 26559.97646;
				e = 0.00779;
				i = 55.84399;
				Ω = 130.19076;
				ω = 143.76357;
				M = 82.75904;
				day = 26;
				month = 8;
				year = 2003;
				hour = 8;
				minute = 45;
				second = 1;
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Help")) {
			if (ImGui::MenuItem("About Author")) {

			}
			if (ImGui::MenuItem("Documentation")) {

			}
			ImGui::EndMenu();
		}
	}
	ImGui::EndMenuBar();

	ImGuiID dockSpaceID = ImGui::GetID("InvisibleWindowDockSpace");
	ImGui::DockSpace(dockSpaceID, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_PassthruCentralNode);

	ImGui::End();

	bool log = false;
	ImGui::Begin("##mainArea", &log, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoDecoration);

	ImTextureID idBackground = pBack;

	static ImDrawList* drawList = ImGui::GetWindowDrawList();
	drawList->AddImage(idBackground, ImVec2(0, 0), ImVec2(1000, 650));

	static const uint32_t num_stars = 32;
	std::mt19937 rng(std::random_device{}());
	static Effect effect[num_stars] = {{ ImGui::GetContentRegionMax(), rng }, { ImGui::GetContentRegionMax(), rng },
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng },{ ImGui::GetContentRegionMax(),rng },
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,
	{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } ,{ ImGui::GetContentRegionMax(),rng } };

	for (uint32_t count = 0; count < num_stars; ++count)
		effect[count].Draw(drawList);

	ImGui::BeginChild("##leftSide", ImVec2(250, ImGui::GetWindowContentRegionMax().y-50));
		ImGui::Dummy(ImVec2(0.0f, ImGui::GetContentRegionMax().y/2-400/2));
		if (ImGui::Button("Home", { 230 - 15,41 }))
			Tab = 1;

		if (start_simulate) {

			ImGui::Dummy(ImVec2(0.0f, 50.0f));
			if (ImGui::Button("Date", { 230 - 15,41 }))
				Tab = 2;

			ImGui::Dummy(ImVec2(0.0f, 50.0f));
			if (ImGui::Button("Graphics", { 230 - 15,41 }))
				Tab = 3;

			ImGui::Dummy(ImVec2(0.0f, 50.0f));
			if (ImGui::Button("3D Model", { 230 - 15,41 }))
				Tab = 4;
		}
	ImGui::EndChild();

	ImGui::SameLine();
	ImGui::Separator();
	ImGui::SameLine();

	ImGui::BeginChild("#rightSide", ImVec2(ImGui::GetWindowContentRegionMax().x-250, ImGui::GetWindowContentRegionMax().y-50));
		if (Tab == 1) {

			ImGui::SetCursorPosX(230);
			
			menu.CustomTextColored(ImColor(255,0,0,255), "Welcome to ASE");

			menu.CustomTextColored(ImColor(255, 0, 0, 255), "		For simulate movement ASE input please a parameter sattelite", 2);

			ImGui::Columns(2, "locations", false);
			{
				ImGui::Spacing();
				menu.CustomTextColored(ImColor(0, 240, 220, 255), "a = ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(0, 240, 220, 255), "e = ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(0, 240, 220, 255), "i = ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(0, 240, 220, 255), "Omega = ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(0, 240, 220, 255), "omega = ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(0, 240, 220, 255), "M = ", 2);
			}
			ImGui::NextColumn();
			{

				ImGui::PushItemWidth(100.0);

				if (ImGui::InputDouble("##inputA", &a)) {
					if (a < 6400.0)
						a = 0.0;
				}
				if (ImGui::InputDouble("##inputE", &e)) {
					if ((e < 0.0)||(e>1.0))
						e = 0.0;
				}
				if (ImGui::InputDouble("##inputI", &i)) {
					if ((i < 0.0) || (i > 180.0))
						i = 0.0;
				}
				if (ImGui::InputDouble("##inputOmega", &Ω)) {
					if ((Ω < 0.0) || (Ω > 180.0))
						Ω = 0.0;
				}
				if (ImGui::InputDouble("##inputοmega", &ω)) {
					if ((ω < 0.0) || (ω > 180.0))
						ω = 0.0;
				}
				if (ImGui::InputDouble("##inputM", &M)) {
					if ((M < 0.0) || (M > 180.0))
						M = 0.0;
				}

				kepler[0] = a;
				kepler[1] = e;
				kepler[2] = i;
				kepler[3] = Ω;
				kepler[4] = ω;
				kepler[5] = M;
			}

			ImGui::Columns();

			menu.CustomTextColored(ImColor(255,0,0, 255), "		Also write a start time Simulate", 2);

			ImGui::Columns(2, "loc", false); {
				ImGui::Spacing();
				menu.CustomTextColored(ImColor(8,255, 0,255), "day: ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(8, 255, 0, 255), "month: ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(8, 255, 0, 255), "year: ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(8, 255, 0, 255), "hour: ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(8, 255, 0, 255), "minute: ", 2);
				ImGui::Dummy(ImVec2(0.0f, 2.0f));

				menu.CustomTextColored(ImColor(8, 255, 0, 255), "second: ", 2);
			}
			ImGui::NextColumn(); {
				if (ImGui::InputInt("##inputDay", &day)) {
					if (day < 1 || day > 31)
						day = 1;
				}
				if (ImGui::InputInt("##inputMonth: ", &month)) {
					if (month < 1 || month > 12)
						month = 1;
				}
				if (ImGui::InputInt("##inputYear: ", &year)) {
					if (year < 2000 || year > 2100)
						year = 2022;
				}
				if (ImGui::InputInt("##inputHour: ", &hour)) {
					if (hour < 1 || hour > 24)
						hour = 1;
				}
				if (ImGui::InputInt("##inputMinute: ", &minute)) {
					if (minute < 1 || minute > 59)
						minute = 1;
				}
				if (ImGui::InputInt("##inputSecont: ", &second)) {
					if (second < 0 || second > 59)
						second = 1;
				}
			}

			ImGui::Columns();

			ImGui::SetCursorPosX(230);

			if (a != 0.0) {
				if (!start_simulate) {
					if (ImGui::Button("Start Simulate")) {
						start_simulate = true;
						
						Kepler kep{ kepler.at(0),kepler.at(1),kepler.at(2) * 3.1415926535 / 180.0,kepler.at(3) * 3.1415926535 / 180.0,kepler.at(4) * 3.1415926535 / 180.0,kepler.at(5) * 3.1415926535 / 180.0 };
						sat.set_Kepler(kep);
						std::thread thread(simulate_thread, year, month, day, hour, minute, second, std::ref(sat), std::ref(*this));
						thread.detach();
					}
				}
				else {
					if (ImGui::Button("End Simulate")) {
						start_simulate = false;
						while (!date.empty())
							date.pop();
						r_vector.erase(r_vector.begin(), r_vector.end());
						t_vector.erase(t_vector.begin(), t_vector.end());
						v_vector.erase(v_vector.begin(), v_vector.end());
					}
				}
			}

		}

		if (start_simulate) {

			{
				std::unique_lock<std::mutex> lock(m);
				if (date.size() > 60) {
					while (!date.empty()) {
						if (r_vector.size() + date.size() > 1440) {
							r_vector.erase(r_vector.begin(), r_vector.end());
							v_vector.erase(v_vector.begin(), v_vector.end());
						}

						std::tuple<Matrix<double>, Matrix<double>, double> buff = date.front();
						date.pop();

						r_vector.push_back(std::get<0>(buff));
						v_vector.push_back(std::get<1>(buff));
						t_vector.push_back(std::get<2>(buff));
					}
				}
			}

			if (Tab == 2) {

				menu.CustomTextColored(ImColor(0, 255, 255, 255), "Date Table");

				static ImVec2 sizeRegion = ImVec2(ImGui::GetContentRegionMax().x-30.0f, ImGui::GetContentRegionMax().y);

				ImGui::BeginChild("#table", sizeRegion);

				if (ImGui::BeginTable("Date", 6, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY)) {
					
					ImGui::TableSetupScrollFreeze(0, 1);
					ImGui::TableSetupColumn("a");
					ImGui::TableSetupColumn("e");
					ImGui::TableSetupColumn("i");
					ImGui::TableSetupColumn("Omega");
					ImGui::TableSetupColumn("omega");
					ImGui::TableSetupColumn("M");
					ImGui::TableHeadersRow();

					if (!r_vector.empty()) {
						for (uint32_t row = 0; row < r_vector.size(); ++row) {
							
							if (row % 60==0) {
								ImGui::TableNextRow();
								sat.set_Kepler({ r_vector.at(row).at(0,0), r_vector.at(row).at(0,1), r_vector.at(row).at(0,2), v_vector.at(row).at(0,0), v_vector.at(row).at(0,1), v_vector.at(row).at(0,2) });
								Kepler tmp = sat.get_kepler();

								for (uint32_t column = 0; column < 6; ++column) {
									ImGui::TableSetColumnIndex(column);
									if(column<2)
										ImGui::TextColored(ImColor(0, 255, 255, 255), "%.2f", (float)tmp.at(column));
									if(column>1)
										ImGui::TextColored(ImColor(0, 255, 255, 255), "%.2f", (float)tmp.at(column)*180.0/3.14159265358979);
								}
							}
						}
					}
					ImGui::EndTable();
				}

				ImGui::EndChild();
			}

			if (Tab == 3) {

				static uint32_t size = 1440;

				static double* x_data = new double[size] {};
				static double* y_data = new double[size] {};
				static double* r_data = new double[size] {};
				static double* t_data = new double[size] {};

				for (uint32_t count = 0; count < r_vector.size(); ++count) {
					x_data[count] = r_vector.at(count).at(0, 0);
					y_data[count] = r_vector.at(count).at(0, 1);
					r_data[count] = sqrt(sq(r_vector.at(count).at(0, 0)) + sq(r_vector.at(count).at(0, 1)) + sq(r_vector.at(count).at(0, 2)));
					t_data[count] = t_vector.at(count);
				}

				if (!r_vector.empty()) {
					for (uint32_t count = r_vector.size(); count < size; ++count) {
						x_data[count] = r_vector.at(r_vector.size() - 1).at(0, 0);
						y_data[count] = r_vector.at(r_vector.size() - 1).at(0, 1);
						r_data[count] = sqrt(sq(r_vector.at(r_vector.size() - 1).at(0, 0)) + sq(r_vector.at(r_vector.size() - 1).at(0, 1)) + sq(r_vector.at(r_vector.size() - 1).at(0, 2)));
						t_data[count] = t_vector.at(r_vector.size() - 1);
					}
				}

				if (ImPlot::BeginPlot("Ellips", ImVec2(500, 250))) {
	
					ImPlot::PlotLine("y(x)", x_data, y_data, size);
					ImPlot::EndPlot();
				}
				
				if (ImPlot::BeginPlot("Sinusoid", ImVec2(500, 250))) {
					ImPlot::PlotLine("r(t)", t_data, r_data, size);
					ImPlot::EndPlot();
				}
			}

			if (Tab == 4) {

				wnd.Gfx().SetCamera(cam.GetMatrix());
				
				//cam.SpawnControlWindow();
				//light.SpawnControlWindow();

				light.Bind(wnd.Gfx(), cam.GetMatrix());
				
				
				for (auto& d : drawables)
				{
					d->Draw(wnd.Gfx());
				}
				light.Draw(wnd.Gfx());

				auto TextureView = wnd.Gfx().m_shaderResourceView;
				ImTextureID my_tex_id = TextureView.Get();
				ImGui::Image(my_tex_id, ImVec2(ImGui::GetContentRegionMax().x, ImGui::GetContentRegionMax().y));

				while (const auto e = wnd.kbd.ReadKey()) {
					if (!e->IsPress())
						continue;
					
					switch (e->GetCode()) {
					case VK_F1:
						if (wnd.CursorEnabled()) {
							wnd.DisableCursor();
							wnd.mouse.EnabledRaw();
						}
						else {
							wnd.EnabledCursor();
							wnd.mouse.DisableRaw();
						}
						break;
					}
				}

				if (!wnd.CursorEnabled()) {
					if (wnd.kbd.KeyIsPressed('W'))
					{
						cam.Translate({ 0.0f,0.0f,dt });
					}
					if (wnd.kbd.KeyIsPressed('A'))
					{
						cam.Translate({ -dt,0.0f,0.0f });
					}
					if (wnd.kbd.KeyIsPressed('S'))
					{
						cam.Translate({ 0.0f,0.0f,-dt });
					}
					if (wnd.kbd.KeyIsPressed('D'))
					{
						cam.Translate({ dt,0.0f,0.0f });
					}
					if (wnd.kbd.KeyIsPressed('R'))
					{
						cam.Translate({ 0.0f,dt,0.0f });
					}
					if (wnd.kbd.KeyIsPressed('F'))
					{
						cam.Translate({ 0.0f,-dt,0.0f });
					}
				}

				while (const auto delta = wnd.mouse.ReadRawDelta())
				{
					cam.Rotate(delta->x, delta->y);
				}

				//if (!TextureView)
				//	TextureView->Release();
			}
		}

	ImGui::EndChild();

	ImGui::End();

	wnd.Gfx().ReturnRenderTarget();

	wnd.Gfx().EndFrame();
}

void simulate_thread(int year, int month, int day, int hour, int minute, int second, Sattelite& sat, App& app)
{
	Solver solver{};
	Time t(year, month, day, hour, minute, second);
	//in day
	double t_1 = t.time_TDB();
	double t_2 = t_1 + 60.0/86400.0;

	//in second
	double step = 1.0;

	Matrix<double> r_start = sat.get_r().get_rectange();
	Matrix<double> v_start = sat.get_velocity().get_rectange();

	//coord Earth
	sky_system earth{ 0,0,0 };
	
	//Coord Moon
	sky_object::Moon _moon;
	ecliptic_system coord_moon_ecl = _moon.coord_Moon(t_1);
	sky_system coord_moon_sky = sky_system{ coord_moon_ecl, t_1 };
	
	//coord Sattelite
	sky_system coord_sat{ 0,0,0 };

	double koef = sqrt(sq(r_start(0, 0)) + sq(r_start(0, 1)) + sq(r_start(0, 2)));

	if (koef < 64000.0)
		koef = 96000.0 / koef;
	else
		koef = 1.0;

	//Интегрирование по минуте
	while (app.SimulateEnabled()) {
		auto modeling = solver.method_Everharts(t_1, t_2,r_start, v_start, step, acc);
		
		//пуш по минуте
		{
			std::unique_lock<std::mutex> lock(m);
			app.get_queue().push(modeling);
		}

		r_start = std::get<0>(modeling);
		v_start = std::get<1>(modeling);

		// Update earth
		app.drawables.at(0)->Update(60.0f / 86400.0f);
		app.drawables.at(0)->UpdateFromCoord(earth, -0.40910517666);

		//Update moon
		coord_moon_ecl = _moon.coord_Moon(t_2);
		coord_moon_sky = sky_system{ coord_moon_ecl, t_2 };
		app.drawables.at(1)->UpdateFromCoord(coord_moon_sky, 0.11658799403);
		app.drawables.at(1)->Update((60.0f/86400.0f)/14.75f);
		
		//Update Sattelie
		Matrix<double> tmp = r_start;
		coord_sat.set_rectange(tmp*koef);
		app.drawables.at(2)->UpdateFromCoord(coord_sat, 0.0);

		//Update time
		t_1 = t_2;
		t_2 = t_1 + 60.0/86400.0;
	}
}

App::~App()
{}


int App::Go()  
{
	while (true)
	{
		// process all messages pending, but to not block for new messages
		if (const auto ecode = Window::ProcessMessages())
		{
			// if return optional has value, means we're quitting so return exit code
			return *ecode;
		}
		DoFrame();
	}
}
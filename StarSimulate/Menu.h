#pragma once
#include <memory>
#include <string>
#include "ChiliWin.h"
#include "imgui/imgui.h"

class Menu
{
public:
	Menu() {
		ImGuiIO& io = ImGui::GetIO();
		ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
		ImFontConfig config;
		config.OversampleH = 1;
		config.OversampleV = 2;
		font1 = io.Fonts->AddFontFromFileTTF("..\\Fonts\\Junicode.ttf", 36, &config);
		font2 = io.Fonts->AddFontFromFileTTF("..\\Fonts\\Junicode-Italic.ttf", 23, &config);
	}

	void SpawnMenu() noexcept;
	void ThemeMenu() noexcept;
	void CustomTextColored(ImColor color, const char* c, uint32_t type=1) noexcept;
private:
	bool active_wnd = true;
	ImFont* font1;
	ImFont* font2;
};

